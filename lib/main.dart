import 'package:flutter/material.dart';
import 'package:menu_makanan/detail_page.dart';
import 'package:menu_makanan/makanan.dart';
import 'package:menu_makanan/style.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    List<Makanan> dataMakanan = Makanan.dummyData;
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Container(
                color: Colors.grey,
                alignment: Alignment.center,
                padding: EdgeInsets.only(top: 10, bottom: 10),
                margin: EdgeInsets.all(20),
                child: Text("Pilih Menu", style: header2),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: dataMakanan.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Image.asset("assets/${dataMakanan[index].gambar}",
                                  width: 150),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(left: 20),
                                      child: Text(
                                        dataMakanan[index].nama,
                                        style: header2,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 20),
                                      child: Text(
                                        dataMakanan[index].deskripsi.toString(),
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20, right: 20),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Rp. ${dataMakanan[index].harga}",
                                            style: header4,
                                          ),
                                          ElevatedButton(
                                            onPressed: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailPage(Makanan(
                                                    dataMakanan[index].nama,
                                                    dataMakanan[index].harga,
                                                    deskripsi:
                                                        dataMakanan[index]
                                                            .deskripsi,
                                                    gambar: dataMakanan[index]
                                                        .gambar,
                                                  )),
                                                ),
                                              );
                                            },
                                            child: Text("Pesan"),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Divider()
                        ],
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
