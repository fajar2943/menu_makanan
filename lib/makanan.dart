class Makanan {
  String nama;
  int harga;
  String gambar;
  String? deskripsi;

  Makanan(this.nama, this.harga,
      {this.gambar = 'makanan.jpeg', this.deskripsi});

  static List<Makanan> dummyData = [
    Makanan("Tempe Bacem", 2000,
        deskripsi: "Tempe yang dibacem", gambar: "tempebacem.jpeg"),
    Makanan("Ayam Bakar", 8000,
        deskripsi: "Ayam bakar kecap", gambar: "ayambakar.jpeg"),
    Makanan("Lontong Tuyuhan", 15000,
        gambar: "opor.jpeg", deskripsi: "Lontong opor + ayam"),
  ];
}
